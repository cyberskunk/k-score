/* global score:false, scoreConfig: false */

let input = []
const template = document.querySelector('.js_row-template')
const target = document.querySelector('.stats-table__items')
const bonuses = document.querySelector('.js_bonuses-value')
const total = document.querySelector('.js_total')
const userInputsElem = document.querySelector('.user-inputs')
const userInputsSelector = '.user-inputs-list__item'
const newGameElem = document.querySelector('.new-game')

function generateRow(item) {
  var clone = template.content.cloneNode(true)
  clone.querySelector('.js_item').textContent = item.item
  clone.querySelector('.js_qty').textContent = item.qty
  clone.querySelector('.js_score').textContent = item.score
  return clone
}

function processInput(inputToProcess) {
  var result = score(inputToProcess, scoreConfig)
  var df = document.createDocumentFragment()
  total.textContent = result.result
  bonuses.textContent = result.result - result.basicResult
  target.innerHTML = ''

  Object.keys(result.items)
    .sort()
    .forEach(k => {
      var _ = {
        item: k,
        qty: result.items[k].qty,
        score: result.items[k].score,
      }

      let row = generateRow(_)
      if (row) {
        df.appendChild(row)
      }
    })
  target.appendChild(df)
}

userInputsElem.addEventListener('click', e => {
  if (e.target.matches(userInputsSelector)) {
    input.push(e.target.dataset.type)
    processInput(input)
  }
})

newGameElem.addEventListener('click', () => {
  input = []
  processInput(input)
})

processInput(input)
