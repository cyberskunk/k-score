module.exports = {
  A: {
    value: 50,
    combo: 3,
    comboValue: 200,
  },
  B: {
    value: 30,
    combo: 2,
    comboValue: 90,
  },
  C: {
    value: 20,
  },
  D: {
    value: 15,
  },
  E: {
    value: 10,
  },
}
