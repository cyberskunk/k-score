const score = require('../score')
let testConfig = require('./testConfig')

describe('scoring function', () => {
  const testInput = ['B', 'A', 'B']
  const testResult = score(testInput, testConfig)
  const expectedResult = {
    basicResult: 110,
    result: 140,
    items: {
      A: {
        qty: 1,
        score: 50,
      },
      B: {
        qty: 2,
        score: 40,
      },
    },
  }

  it('provides correct basicResult', () => {
    expect(testResult.basicResult).toBe(expectedResult.basicResult)
  })

  it('provides correct result', () => {
    expect(testResult.result).toBe(expectedResult.result)
  })

  it('provides correct "A" count', () => {
    expect(testResult.items.A.qty).toBe(expectedResult.items.A.qty)
  })

  it('provides correct "B" count', () => {
    expect(testResult.items.B.qty).toBe(expectedResult.items.B.qty)
  })

  it('does not depend on input order', () => {
    const anoterTestInput = ['B', 'B', 'A']
    const anotherTestResult = score(anoterTestInput, testConfig)

    expect(testResult).toEqual(anotherTestResult)
  })
})
