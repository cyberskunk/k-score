/**
 * config:
 *  {
 *      items: [
 *          'A': {
 *              value: 50,
 *              combo: 3,
 *              comboValue: 200
 *          },
 *          ...
 *      ]
 */

function score(input, config) {
  const inputHash = input.reduce((hash, item) => {
    if (typeof hash[item] === 'undefined') {
      hash[item] = {
        qty: 1,
      }
    } else {
      hash[item].qty += 1
    }

    return hash
  }, {})

  const userInputKeys = Object.keys(inputHash)

  userInputKeys.forEach(k => {
    inputHash[k].score = inputHash[k].qty * config[k].value
  })

  const basicResult = userInputKeys.reduce((_result, item) => _result + inputHash[item].score, 0)

  const result = userInputKeys.reduce((_result, item) => {
    const comboLength = config[item].combo
    let count = inputHash[item].qty
    let itemScore = 0

    while (count >= comboLength) {
      itemScore += config[item].comboValue
      count -= comboLength
    }

    return _result + itemScore + count * config[item].value
  }, 0)

  return {
    result,
    basicResult,
    items: inputHash,
  }
}

if (typeof module === 'object' && module.exports) {
  module.exports = score
}
