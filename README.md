## Build and run

The application does not use any build system yet,
so it is sufficient to serve app folder by any static server
(e.g. `python -m SimpleHTTPServer 8080` to run basic HTTP server on port 8080 using python)

## Test

There are some basic tests which use [Jest][jest]

    npm i && npm t

## Codestyle

[Eslint][1] and [Prettier][2] are used, npm-run action is available: `npm run lint`

Use `npm run lint-fix` to  fix linting errors by linter(keep in mind, that not all of them may be fixed automatically)

## Public demo

Try `http://k-score.surge.sh`

[jest]: https://facebook.github.io/jest/
[1]: https://eslint.org/
[2]: https://prettier.io/